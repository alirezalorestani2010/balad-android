package com.example.baladandroid

import com.example.baladandroid.musicplayer.exceptions.MusicOperationException
import com.example.baladandroid.musicplayer.model.MusicListItemFactoryImpl
import com.example.baladandroid.musicplayer.player.PremiumPlayer
import com.example.baladandroid.musicplayer.player.state.PauseState
import com.example.baladandroid.musicplayer.player.state.PlayingState
import com.example.baladandroid.musicplayer.player.state.StopState
import org.junit.Test
import org.junit.Assert.*


class PremiumPlayerUnitTest {

    @Test
    fun getCurrentMusicTest() {
        val premiumPlayer = PremiumPlayer(MusicListItemFactoryImpl().createMusicListItems())
        assertEquals(
            premiumPlayer.playList[premiumPlayer.currentMusicIndex],
            premiumPlayer.getCurrentMusic()
        )
    }

    @Test
    fun nextTrackTest() {
        val premiumPlayer = PremiumPlayer(MusicListItemFactoryImpl().createMusicListItems())
        assertEquals(
            premiumPlayer.playList[premiumPlayer.currentMusicIndex + 1],
            premiumPlayer.nextMusic()
        )
    }

    @Test
    fun playOperationTest() {
        val premiumPlayer = PremiumPlayer(MusicListItemFactoryImpl().createMusicListItems())
        premiumPlayer.play()
        assertEquals(
            true,
            premiumPlayer.state is PlayingState
        )
    }

    @Test
    fun pauseOperationTest() {
        val premiumPlayer = PremiumPlayer(MusicListItemFactoryImpl().createMusicListItems())
        premiumPlayer.pause(2)
        assertEquals(
            true,
            premiumPlayer.state is PauseState
        )
    }

    @Test
    fun checkCurrentTimeAfterResumeOperationTest() {
        val premiumPlayer = PremiumPlayer(MusicListItemFactoryImpl().createMusicListItems())
        premiumPlayer.pause(2)
        assertEquals(
            2,
            premiumPlayer.play()
        )
    }


    @Test
    fun stopOperationTest() {
        val premiumPlayer = PremiumPlayer(MusicListItemFactoryImpl().createMusicListItems())
        premiumPlayer.stop()
        assertEquals(
            true,
            premiumPlayer.state is StopState
        )
    }

    @Test(expected = MusicOperationException::class)
    fun playWhilePlayingTest() {
        val premiumPlayer = PremiumPlayer(MusicListItemFactoryImpl().createMusicListItems())
        premiumPlayer.play()
    }




}