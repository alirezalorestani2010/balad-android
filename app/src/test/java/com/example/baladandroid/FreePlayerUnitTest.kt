package com.example.baladandroid

import com.example.baladandroid.musicplayer.exceptions.UserLevelException
import com.example.baladandroid.musicplayer.model.Music
import com.example.baladandroid.musicplayer.model.MusicListItemFactoryImpl
import com.example.baladandroid.musicplayer.player.FreePlayer
import com.example.baladandroid.musicplayer.player.PremiumPlayer
import org.junit.Test

class FreePlayerUnitTest {

    @Test(expected = UserLevelException::class)
    fun userLevelErrorHandlingTest() {
        val freePlayer = FreePlayer(MusicListItemFactoryImpl().createMusicListItems())
        freePlayer.addMusicAsNext(Music("test", "test", 2))
    }
}