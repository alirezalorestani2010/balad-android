package com.example.baladandroid

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.baladandroid.databinding.ItemMusicListBinding
import com.example.baladandroid.musicplayer.model.Music

class MusicListRecyclerAdapter() :
    ListAdapter<Music, MusicListRecyclerAdapter.RecyclerViewHolder>(object :
        DiffUtil.ItemCallback<Music>() {
        override fun areItemsTheSame(oldItem: Music, newItem: Music): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: Music, newItem: Music): Boolean {
            return true
        }

    }) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val mBinding: ItemMusicListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_music_list,
            parent,
            false
        )

        return RecyclerViewHolder(mBinding)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        val item = getItem(position)
        holder.uBinding.music = item
        holder.itemView.tag = item
    }

    inner class RecyclerViewHolder(val uBinding: ItemMusicListBinding) :
        RecyclerView.ViewHolder(uBinding.root)


}
