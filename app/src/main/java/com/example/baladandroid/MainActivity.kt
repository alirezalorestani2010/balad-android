package com.example.baladandroid

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.baladandroid.databinding.ActivityMainBinding
import com.example.baladandroid.musicplayer.model.Music
import com.example.baladandroid.musicplayer.model.MusicListItemFactory
import com.example.baladandroid.musicplayer.model.MusicListItemFactoryImpl

class MainActivity : AppCompatActivity() {
    lateinit var recyclerViewAdapter: MusicListRecyclerAdapter
    val musicListItemFactory: MusicListItemFactory by lazy { MusicListItemFactoryImpl() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.apply {
            rvMusicListActivityMain.setHasFixedSize(true)
            rvMusicListActivityMain.layoutManager =
                LinearLayoutManager(this@MainActivity, LinearLayoutManager.VERTICAL, false)
            recyclerViewAdapter = MusicListRecyclerAdapter()
            rvMusicListActivityMain.adapter = recyclerViewAdapter
            recyclerViewAdapter.submitList(
                musicListItemFactory.createMusicListItems()
            )
        }
    }
}