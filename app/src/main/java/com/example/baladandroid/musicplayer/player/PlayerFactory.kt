package com.example.baladandroid.musicplayer.player

import com.example.baladandroid.musicplayer.model.Music
import com.example.baladandroid.musicplayer.model.User

class PlayerFactory {
    fun create(user: User, playList: List<Music>): Player =
        if (user.isPremium)
            PremiumPlayer(playList)
        else
            FreePlayer(playList)
}