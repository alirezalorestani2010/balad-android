package com.example.baladandroid.musicplayer.model

data class Music(val name: String, val Artist: String, val duration: Int)

interface MusicListItemFactory {
    fun createMusicListItems(): List<Music>
}

class MusicListItemFactoryImpl : MusicListItemFactory {
    override fun createMusicListItems(): List<Music> {
        return listOf(
            Music("take me to church", "hozier", 3),
            Music("Drivers License", "Olivia Rodrigo", 3),
            Music("Leave the Door Open", "Silk Sonic", 3),
            Music("Save Your Tears", "The Weeknd & Ariana Grande", 3),
            Music("Levitating", "Dua Lipa feat. DaBaby", 3),
            Music("Telepatía", "Kali Uchis", 3),
            Music("Back in Blood", "Pooh Shiesty feat. Lil Durk", 3),
            Music("Good Days", "SZA", 3),
            Music("Peaches", " Justin Bieber feat. Daniel Caesar & Giveon", 3),
            Music("Montero (Call Me By Your Name)", "Lil Nas X", 3),
            Music("Serotonin", "Girl in Red", 3),
            Music("Up", "Cardi B", 3),
            Music( "Your Power", "Billie Eilish", 3),
            Music( "Good 4 U", "Olivia Rodrigo", 3),
            Music( "Fight For You", "H.E.R.", 3)
        )
    }

}
