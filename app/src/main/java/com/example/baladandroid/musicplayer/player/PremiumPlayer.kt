package com.example.baladandroid.musicplayer.player

import com.example.baladandroid.musicplayer.model.Music

class PremiumPlayer(playList: List<Music>) : Player(playList) {

    override fun replaceMusicList(musicList: List<Music>) {
        playList.removeAll(playList.subList(currentMusicIndex, playList.lastIndex))
        playList.addAll( musicList.toMutableList())
    }

    override fun previousMusic(): Music {
        state.stopMusic()
        if (currentMusicIndex > 0) {
            currentMusicIndex--

        } else {
            currentMusicIndex = playList.lastIndex

        }
        state.playMusic()
        return playList[currentMusicIndex]
    }

    override fun addMusicAsNext(music: Music) {
        playList.add(currentMusicIndex + 1, music)
    }


}