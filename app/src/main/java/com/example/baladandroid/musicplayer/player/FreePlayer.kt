package com.example.baladandroid.musicplayer.player

import com.example.baladandroid.musicplayer.exceptions.MusicOperationException
import com.example.baladandroid.musicplayer.exceptions.UserLevelException
import com.example.baladandroid.musicplayer.model.Music

class FreePlayer(playList: List<Music>) : Player(playList) {
    override fun replaceMusicList(musicList: List<Music>) {
        if (musicList.size >= 5) {
            playList.removeAll(playList.subList(currentMusicIndex, playList.lastIndex))
            playList.addAll(musicList.toMutableList().shuffled())
        } else {
            throw MusicOperationException("Free members are not allowed to replace less than 5 songs")
        }
    }

    override fun previousMusic(): Music {
        throw UserLevelException()
    }

    override fun addMusicAsNext(music: Music) {
        throw UserLevelException()
    }
}