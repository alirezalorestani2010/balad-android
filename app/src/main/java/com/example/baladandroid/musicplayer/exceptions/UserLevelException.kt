package com.example.baladandroid.musicplayer.exceptions

class UserLevelException() : Exception() {
    override val message: String
        get() = "Free members are not allowed to do this operation"
}