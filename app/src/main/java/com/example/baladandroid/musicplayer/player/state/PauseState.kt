package com.example.baladandroid.musicplayer.player.state

import com.example.baladandroid.musicplayer.exceptions.MusicOperationException
import com.example.baladandroid.musicplayer.player.Player

class PauseState(private val player: Player) : MusicState {

    override fun pauseMusic(currentTime: Int) {
        throw MusicOperationException("Already Paused")
    }

    override fun playMusic(): Int {
        player.state = player.playingState
        println("Start Playing")
        return player.currentMusicTime
    }

    override fun stopMusic() {
        player.state = player.stopState
        player.resetCurrentMusicTime()
        println("Start Stopping")

    }

    override fun toString(): String {
        return "Paused"
    }
}