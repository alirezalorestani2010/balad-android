package com.example.baladandroid.musicplayer.player.state

import com.example.baladandroid.musicplayer.exceptions.MusicOperationException
import com.example.baladandroid.musicplayer.player.Player

class PlayingState(private val player: Player) : MusicState {

    override fun pauseMusic(currentTime: Int) {
        player.state = player.pauseState
        player.currentMusicTime = currentTime
        println("Start Pausing")
    }

    override fun playMusic(): Int {
        throw MusicOperationException("can't resume in playing state")
    }

    override fun stopMusic() {
        player.state = player.stopState
        player.resetCurrentMusicTime()
        println("Start Stopping")
    }

    override fun toString(): String {
        return "Playing"
    }
}