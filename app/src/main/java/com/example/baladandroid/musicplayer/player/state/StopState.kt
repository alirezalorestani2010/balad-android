package com.example.baladandroid.musicplayer.player.state

import com.example.baladandroid.musicplayer.exceptions.MusicOperationException
import com.example.baladandroid.musicplayer.player.Player

class StopState(private val player: Player) : MusicState {

    override fun pauseMusic(currentTime: Int) {
        throw MusicOperationException("can't pause in Stopped State")
    }

    override fun playMusic(): Int {
        player.state = player.playingState
        println("Start Playing")
        return player.currentMusicTime
    }

    override fun stopMusic() {
        throw MusicOperationException("Already stopped")
    }

    override fun toString(): String {
        return "Stopped"
    }
}