package com.example.baladandroid.musicplayer.exceptions;

class MusicOperationException(message: String) : Exception(message)
