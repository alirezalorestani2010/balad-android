package com.example.baladandroid.musicplayer.player

import com.example.baladandroid.musicplayer.model.Music
import com.example.baladandroid.musicplayer.player.state.MusicState
import com.example.baladandroid.musicplayer.player.state.PauseState
import com.example.baladandroid.musicplayer.player.state.PlayingState
import com.example.baladandroid.musicplayer.player.state.StopState

abstract class Player(musicList: List<Music>) {

    val playList: MutableList<Music> = musicList.toMutableList()

    val playingState: PlayingState
        get() = PlayingState(this@Player)

    val stopState: StopState
        get() = StopState(this@Player)

    val pauseState: PauseState
        get() = PauseState(this@Player)

    var state: MusicState = PlayingState(this@Player)

    var currentMusicIndex = 0
    var currentMusicTime = 0

    fun resetCurrentMusicTime() {
        currentMusicTime = 0
    }

    fun getCurrentMusic(): Music = playList[currentMusicIndex]

    fun getRemainingMusics(): List<Music> = playList.subList(currentMusicIndex, playList.lastIndex)

    abstract fun replaceMusicList(musicList: List<Music>)

    fun pause(currentMusicTime: Int) {
        state.pauseMusic(currentMusicTime)
    }

    fun play(): Int =
        state.playMusic()

    fun stop() {
        state.stopMusic()
    }

    fun nextMusic(): Music {
        state.stopMusic()
        if (currentMusicIndex < playList.lastIndex) {
            currentMusicIndex++

        } else {
            currentMusicIndex = 0

        }
        state.playMusic()
        return playList[currentMusicIndex]
    }


    abstract fun previousMusic(): Music

    abstract fun addMusicAsNext(music: Music)

}