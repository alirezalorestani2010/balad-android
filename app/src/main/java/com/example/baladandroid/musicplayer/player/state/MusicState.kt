package com.example.baladandroid.musicplayer.player.state

interface MusicState {
    fun pauseMusic(currentTime: Int)
    fun playMusic(): Int
    fun stopMusic()
}
