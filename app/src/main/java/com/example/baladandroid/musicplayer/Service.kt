package com.example.baladandroid.musicplayer

import com.example.baladandroid.musicplayer.model.Music
import com.example.baladandroid.musicplayer.model.User
import com.example.baladandroid.musicplayer.player.Player
import com.example.baladandroid.musicplayer.player.PlayerFactory

class Service(user: User, playList: List<Music>) {
    val player: Player = PlayerFactory().create(user, playList)
}