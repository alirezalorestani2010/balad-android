# BALAD Android Project
[APK Here](https://gitlab.com/alirezalorestani2010/balad-android/-/blob/de87a73d86a8edb93b644f7d12e0dad7cf9c00db/app-debug.apk)
## _section 1_

* In this section I implement a music service : Service.kt placed in musicplayer directory
* in this class by using **factory design pattern**, we create an instance of our player by calling create method of PlayerFactory class.
* based on user level (Free/Premium), Factory decide to create which version of Player.
* there are  two types of player which both extends from Player Class: FreePlayer and PremiumPlayer.
* the common operations are implemented in super class (Player) and others are implemented in child classes.
* I use **state design pattern** for implementing these three operations: play, pause and stop due to they change the state of player object.
* I create two types of custom exception for user level errors and music operation errors.
* I decide to use list(mutable list) for representing playlist. because we need to iterate on our list and also need to access items randomly. 


## _section 2_

* I implement the RecyclerView and the way it scroll based on my understanding, let me know if you looking for something else.

## _unit testing_

* I test some operations in two test classes.
* common functionalities are tested in PremiumPlayerUnitTest class and not rewrited in other one.
